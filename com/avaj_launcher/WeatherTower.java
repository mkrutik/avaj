package com.avaj_launcher;

import com.avaj_launcher.aircraft.Aircraft;
import com.avaj_launcher.tower.Tower;
import com.avaj_launcher.weather.WeatherProvider;

public class WeatherTower extends Tower {

    public String getWeather(Aircraft.Coordinates coordinates) throws Exception {
        String weather = WeatherProvider.getProvider().getCurrentWeather(coordinates);

        return weather;
    }

    void changeWeather() throws Exception {
        this.conditionChanged();
    }
}
