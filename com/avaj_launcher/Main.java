package com.avaj_launcher;

import com.avaj_launcher.Log.Log;

import java.util.List;

public class Main {
    public static void main(String... args) {
        try {
            if (args.length != 1) {
                System.out.println("Error: File name argument absent");
                System.out.println("Please run program with argument - path to scenario file");
            }

            List<String[]> data = GetInput.getInput(args[0]);
            Simulation.simulation(data);

            Log.endLog();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
