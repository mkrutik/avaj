package com.avaj_launcher;

import com.avaj_launcher.Log.Log;
import com.avaj_launcher.aircraft.AircraftFactory;
import com.avaj_launcher.flyable.Flyable;

import java.util.List;

public class Simulation {
    public static void simulation(List<String[]> data) throws Exception {
        try {
            int iteration = Integer.parseInt(data.get(0)[0]);
            data.remove(0);

            WeatherTower tower = new WeatherTower();

            for (String[] line : data) {
                Flyable f = AircraftFactory.newAircraft(line[0], line[1], Integer.parseInt(line[2]), Integer.parseInt(line[3]), Integer.parseInt(line[4]));
                f.registerTower(tower);
            }

            for (int i = 1; i <= iteration; i++) {
                Log.addLog("--------------------------------------- " + "[" + i + "]");
                tower.changeWeather();
            }
            Log.addLog("        -* The End :) *-");
        } catch (Exception  e) {
            System.out.println(e.getMessage() + "!!!!!!!!!!!!!!!!!!!");
            Log.addLog(e.getMessage());
        }
    }
}
