package com.avaj_launcher.tower;


import com.avaj_launcher.exception.CustomException;
import com.avaj_launcher.flyable.Flyable;

import java.util.LinkedList;
import java.util.List;

public class Tower {
    private List<Flyable> observers = new LinkedList<Flyable>();

    public void register(Flyable flyable) throws Exception {
        if (flyable == null) {
            throw new CustomException("Tower: failed to register Flyable - invalid parameter");
        } else if (observers.contains(flyable)) {
            throw new CustomException("Tower: failed to register Flyable - already registered");
        }

        observers.add(flyable);
    }

    public void unregister(Flyable flyable) throws Exception {
        if (!observers.contains(flyable)) {
            throw new CustomException("Tower: failed to unregistered - unknown flyable");
        }

        observers.remove(flyable);
    }

    protected void conditionChanged() throws Exception {
        List<Flyable> tmp = new LinkedList<Flyable>(observers);

        for (final Flyable flyable : tmp) {
            flyable.updateConditions();
        }
    }
}
