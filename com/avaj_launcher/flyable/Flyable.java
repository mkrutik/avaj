package com.avaj_launcher.flyable;

import com.avaj_launcher.WeatherTower;

public interface Flyable {

    public void updateConditions() throws Exception;
    public void registerTower(WeatherTower weatherTower) throws Exception;
}
