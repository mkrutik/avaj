package com.avaj_launcher;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

public class GetInput {

    private static BufferedReader br = null;
    private static final String reg = "^(Baloon|JetPlane|Helicopter)\\s*\\S*(\\s*\\d*){3}\\s*$";
    private static final String reg_first = "^\\d*\\s*$";


    public static List<String[]> getInput(String file_name) throws Exception {
        if (br == null)
            br = new BufferedReader(new FileReader(file_name));
        List<String> validated = GetInput.validation();

        List<String[]> res = parse(validated);

        return res;
    }

    private static List<String> validation() throws Exception {
        List<String> lines = new ArrayList<String>();
        String line;

        while ((line = br.readLine()) != null) {
            if ((lines.size() == 0 && !line.matches(reg_first)) || (lines.size() != 0 && !line.matches(reg))) {
                throw new Exception("Invalid file content\n" + line);
            }

            lines.add(line);
        }

        return lines;
    }

    private static List<String[]> parse(List<String> lines) {
        List<String[]> res = new ArrayList<String[]>();

        int counter = 0;

        while (counter < lines.size()) {
            if (counter == 0) {
                res.add(new String[] {lines.get(counter)});
            } else {
                res.add(lines.get(counter).split("\\s+"));
            }

            ++counter;
        }

        return res;
    }

    // TODO: implement decrypt
}
