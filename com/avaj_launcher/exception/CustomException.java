package com.avaj_launcher.exception;

public class CustomException extends Exception {
    public CustomException(String message) {
        super(message);
    }
}
