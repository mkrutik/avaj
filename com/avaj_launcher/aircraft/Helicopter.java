package com.avaj_launcher.aircraft;

import com.avaj_launcher.Log.Log;
import com.avaj_launcher.WeatherTower;
import com.avaj_launcher.flyable.Flyable;

public class Helicopter extends Aircraft implements Flyable  {
    private WeatherTower weatherTower;

    Helicopter(String name, Coordinates coordinates) throws Exception {
        super(name, coordinates);
    }

    @Override
    public void updateConditions() throws Exception {
        final String weather = weatherTower.getWeather(this.coordinates);

        switch (weather) {
            case "SUN":
                this.coordinates.updateLongitude(10);
                this.coordinates.updateHeight(2);
                this.log("The sunlight hurts my eyes");
                break;
            case "RAIN":
                this.coordinates.updateLongitude(5);
                this.log("The sunlight hurts my eyes");
                break;
            case "FOG":
                this.coordinates.updateLongitude(1);
                this.log("They all disappear when we're fog-dancing");
                break;
            case "SNOW":
                this.coordinates.updateHeight(-12);
                this.log("On a night with a gentle falling snow");
                break;
        }

        if (this.coordinates.getHeight() == 0) {
            this.weatherTower.unregister(this);
            Log.addLog("Tower says: Helicopter#" + this.name + "(" + this.id + ") unregistered from weather tower."
                    + " POS : (" + this.coordinates.getLongitude() + ";" + this.coordinates.getLatitude() + ")");
        }
    }

    @Override
    public void registerTower(WeatherTower weatherTower) throws Exception {
        this.weatherTower = weatherTower;
        this.weatherTower.register(this);
        Log.addLog("Tower says: Helicopter#" + this.name + "(" + this.id + ") registered to weather tower.");
    }
}