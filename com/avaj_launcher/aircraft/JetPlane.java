package com.avaj_launcher.aircraft;

import com.avaj_launcher.Log.Log;
import com.avaj_launcher.flyable.Flyable;
import com.avaj_launcher.WeatherTower;

public class JetPlane extends Aircraft implements Flyable {
    private WeatherTower weatherTower;

    JetPlane(String name, Coordinates coordinates) throws Exception {
        super(name, coordinates);
    }

    @Override
    public void updateConditions() throws Exception {
        final String weather = weatherTower.getWeather(this.coordinates);

        switch (weather) {
            case "SUN":
                this.coordinates.updateLatitude(10);
                this.coordinates.updateHeight(2);
                this.log("The sunlight hurts my eyes");
                break;
            case "RAIN":
                this.coordinates.updateLatitude(5);
                this.log("Let it rain, let it rain.");
                break;
            case "FOG":
                this.coordinates.updateLatitude(1);
                this.log("When that fog horn blows you know I will be coming home");
                break;
            case "SNOW":
                this.coordinates.updateHeight(-7);
                this.log("Let it snow, let it snow, let it snow");
                break;
        }

        if (this.coordinates.getHeight() == 0) {
            this.weatherTower.unregister(this);
            Log.addLog("Tower says: JetPlane#" + this.name + "(" + this.id + ") unregistered from weather tower."
                    + " POS : (" + this.coordinates.getLongitude() + ";" + this.coordinates.getLatitude() + ")");
        }
    }

    @Override
    public void registerTower(WeatherTower wheatherTower) throws Exception {
        this.weatherTower = wheatherTower;
        this.weatherTower.register(this);

        Log.addLog("Tower says: JetPlane#" + this.name + "(" + this.id + ") registered to weather tower.");
    }
}