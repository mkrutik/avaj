package com.avaj_launcher.aircraft;

import com.avaj_launcher.exception.CustomException;
import com.avaj_launcher.flyable.Flyable;

public class AircraftFactory {

    public static Flyable newAircraft(String type, String name, int longitude, int latitude, int height) throws Exception {
        Flyable flyable = null;
        Aircraft.Coordinates coordinates = new Aircraft.Coordinates(longitude, latitude, height);

        switch (type) {
            case "Helicopter":
                flyable = new Helicopter(name, coordinates);
                break;
            case "JetPlane":
                flyable = new JetPlane(name, coordinates);
                break;
            case "Baloon":
                flyable = new Baloon(name, coordinates);
                break;
            default:
                throw new CustomException("AircraftFctory: filed to create Flyable - unknown type");
            }

        return flyable;
    }
}
