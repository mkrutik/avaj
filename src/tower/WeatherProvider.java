package tower;

import aircraft.*;
import simulation.*;

public class WeatherProvider {

    private String[] weather;

    private static WeatherProvider weatherProvider = null;

    private WeatherProvider() {
        weather = new String[] {"RAIN", "FOG", "SUN", "SNOW"};
    }

    public static WeatherProvider getProvider() {
        if (weatherProvider == null) {
            weatherProvider = new WeatherProvider();
        }

        return weatherProvider;
    }

    public final String getCurrentWeather(final Coordinates pos) throws CustomException {
        if (pos == null) {
            throw new CustomException("WeatherProvider: failed to get current weather - invalid parametr");
        }

        long tmp = pos.getHeight() + pos.getLatitude() + pos.getLongitude();
        long rand = (long)(Math.random() * 100);

        int index = (int)((tmp + rand) % 4);
        return weather[index];
    }
}
