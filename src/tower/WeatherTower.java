package tower;

import aircraft.*;

public class WeatherTower extends Tower {

    public String getWeather(Coordinates coordinates) throws Exception {
        String weather = WeatherProvider.getProvider().getCurrentWeather(coordinates);

        return weather;
    }

    void changeWeather() throws Exception {
        this.conditionChanged();
    }
}
