package simulation;

import java.io.FileOutputStream;

public class Log {
    private static FileOutputStream file;
    private static Log log = new Log();

    private Log() {
        try {
            file = new FileOutputStream("simulation.txt", false);
        } catch (Exception e) {}
    }

    public static void addLog(String message) throws CustomException {
        try {
            file.write(message.getBytes());
            file.write("\n".getBytes());
        } catch (Exception e) {
            throw new CustomException("Log: failed to log" + e.getMessage());
        }
    }

    public static void endLog() throws CustomException {
        try {
            file.close();
        } catch (Exception e) {
            throw new CustomException("Failed to close the file" + e.getMessage());
        }
    }
}
