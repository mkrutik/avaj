package simulation;

import aircraft.*;
import tower.*;

import java.util.List;

public class Simulation {

    public static void main(String... args) {
        try {
            if (args.length != 1) {
                System.out.println("Error: File name argument absent");
                System.out.println("Please run program with argument - path to scenario file");
            }

            List<String[]> data = GetInput.getInput(args[0]);
            Simulation.simulation(data);

            Log.endLog();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public static void simulation(List<String[]> data) throws Exception {
        try {
            int iteration = Integer.parseInt(data.get(0)[0]);
            data.remove(0);

            WeatherTower tower = new WeatherTower();

            for (String[] line : data) {
                Flyable f = AircraftFactory.newAircraft(line[0], line[1], Integer.parseInt(line[2]), Integer.parseInt(line[3]), Integer.parseInt(line[4]));
                f.registerTower(tower);
            }

            for (int i = 1; i <= iteration; i++) {
                Log.addLog("--------------------------------------- " + "[" + i + "]");
                WeatherTowerProxy.changeWeather(tower);
            }
            Log.addLog("        -* The End :) *-");
        } catch (Exception  e) {
            Log.addLog(e.getMessage());
        }
    }
}
