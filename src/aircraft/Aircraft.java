package aircraft;

import simulation.*;

public class Aircraft {
    protected long id;
    protected String name;
    protected Coordinates coordinates;

    private static long idCounter = 0;

    protected Aircraft(String name, Coordinates coordinates) throws Exception {
        if (name == null || name.isEmpty() || coordinates.getHeight() < 0 || coordinates.getHeight() > 100) {
            throw new CustomException("Aircreaft: failed to create new instanse - invalid parameters");
        }

        this.id = nextId();
        this.name = name;
        this.coordinates = coordinates;
    }

    private long nextId() {
        return ++idCounter;
    }

    protected void log(String message) throws Exception {
        Log.addLog(this.getClass().getSimpleName() + "#" + this.name + "(" + this.id + "): " + message);
    }
}
