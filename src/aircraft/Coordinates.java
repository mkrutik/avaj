package aircraft;

import simulation.*;

public class Coordinates {
    private int longitude;
    private int latitude;
    private int height;

    Coordinates(final int longitude, final int latitude, final int height) throws  Exception {
        if (longitude < 0 || latitude < 0 || height < 0) {
            throw  new CustomException("Coordinates: failed to create new instanse - invalid parameters");
        }

        this.longitude = longitude;
        this.latitude = latitude;
        this.height = height;
    }

    public int getLongitude() {
        return this.longitude;
    }

    public int getLatitude() {
        return  this.latitude;
    }

    public int getHeight() {
        return this.height;
    }

    public void updateLongitude(int longitude) {
        this.longitude += longitude;
    }

    public void updateLatitude(int latitude) {
        this.latitude += latitude;
    }

    public void updateHeight(int height) {
        this.height += height;

        if (this.height > 100) {
            this.height = 100;
        }
        if (this.height < 0) {
            this.height = 0;
        }
    }
}