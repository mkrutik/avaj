package aircraft;

import simulation.*;
import tower.*;

public class Baloon extends Aircraft implements Flyable {
    private WeatherTower weatherTower;

    Baloon(String name, Coordinates coordinates) throws Exception {
        super(name, coordinates);
    }

    @Override
    public void updateConditions() throws Exception {
        final String weather = weatherTower.getWeather(this.coordinates);

        switch (weather) {
            case "SUN":
                this.coordinates.updateLongitude(2);
                this.coordinates.updateHeight(4);
                this.log("Sun shine bright like a diamond");
                break;
            case "RAIN":
                this.coordinates.updateLatitude(-5);
                this.log("And I'll keep raining, raining, raining over you");
                break;
            case "FOG":
                this.coordinates.updateHeight(-3);
                this.log("This dark fog will appear");
                break;
            case "SNOW":
                this.coordinates.updateHeight(-15);
                this.log("Walking like you're sucking velvet snow");
                break;
        }

        if (this.coordinates.getHeight() == 0) {
            this.weatherTower.unregister(this);
            Log.addLog("Tower says: Baloon#" + this.name + "(" + this.id + ") unregistered from weather tower."
                    + " POS : (" + this.coordinates.getLongitude() + ";" + this.coordinates.getLatitude() + ")");
        }
    }

    @Override
    public void registerTower(WeatherTower weatherTower) throws Exception {
        this.weatherTower = weatherTower;
        this.weatherTower.register(this);

        Log.addLog("Tower says: Baloon#" + this.name + "(" + this.id + ") registered to weather tower.");
    }
}