package aircraft;

import tower.*;

public interface Flyable {

    public void updateConditions() throws Exception;
    public void registerTower(WeatherTower weatherTower) throws Exception;
}
